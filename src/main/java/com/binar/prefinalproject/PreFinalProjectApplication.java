package com.binar.prefinalproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PreFinalProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PreFinalProjectApplication.class, args);
	}

}
